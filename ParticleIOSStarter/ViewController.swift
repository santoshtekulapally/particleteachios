//
//  ViewController.swift
//  ParticleIOSStarter
//
//  Created by Parrot on 2019-06-29.
//  Copyright © 2019 Parrot. All rights reserved.

import UIKit
import Particle_SDK

class ViewController: UIViewController {

    @IBOutlet weak var answerlabel: UILabel!
    @IBOutlet weak var Structimg: UIImageView!
    @IBOutlet weak var ques1textview: UITextView!
    
    @IBOutlet weak var ques2textview: UITextView!
    
    var ques1 = true


    
    
    @IBOutlet weak var scorelabel: UILabel!
    // MARK: User variables
    let USERNAME = "santoshonthemove@gmail.com"
    let PASSWORD = "santosh@123"
    
    // MARK: Device
    // Jenelle's device
    //let DEVICE_ID = "36001b001047363333343437"
    // Antonio's device
    let DEVICE_ID = "25001a000f47363333343437"
    var myPhoton : ParticleDevice?

    // MARK: Other variables
    var gameScore:Int = 0

    
    // MARK: Outlets
    
   // @IBOutlet weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        
        self.ques1 = true
        
        super.viewDidLoad()
        self.Structimg.image = UIImage(named:"triangle")
        
    self.ques2textview.isHidden = false
        self.ques1textview.isHidden = true

         self.answerlabel.text = "Please press the button on particle"
        
        self.scorelabel.text = "0"

        // 1. Initialize the SDK
        ParticleCloud.init()
 
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")

                // try to get the device
                self.getDeviceFromCloud()

            }
        } // end login
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: Get Device from Cloud
    // Gets the device from the Particle Cloud
    // and sets the global device variable
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
                // subscribe to events
                


                self.subscribeToParticleEvents()
                
               // self.nextquestion()


            }
            
        } // end getDevice()
    }
    
    
    //MARK: Subscribe to "playerChoice" events on Particle
    func subscribeToParticleEvents() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "playerChoice",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
            
            if let _ = error {
                print("could not subscribe to events")
            } else {
                print("got event with data \(event?.data)")
                let choice = (event?.data)!
                
                //buton 4 action
                if (choice == "A") {
                    
                    if(self.ques1==true) {
                        self.turnParticleRed()
                        DispatchQueue.main.async {
                            self.answerlabel.text = "OOoooppsss!!! Please Press on the right button !!"
                        }
                        
                    }
                    else if (self.ques1==false)
                    {
                        self.turnParticleGreen()
                        self.gameScore = self.gameScore + 1;
                        DispatchQueue.main.async {
                            
                            self.answerlabel.text = "YaaaY ...! your answer is correct !!"

                            self.scorelabel.text = "Score is " + String(self.gameScore)
                        }
                        
                    }
                    
 
                }
                    
                    //buton 2 action
                else if (choice == "B") {
                    
                    if(self.ques1==true) {
                        
                        self.turnParticleRed()
                        
                         self.answerlabel.text = "OOoooppsss!!! Please Press on the right button !!"
                    }
                    else if (self.ques1==false)
                    {
                       
                       
                             self.turnParticleRed()
                        DispatchQueue.main.async {
                        self.answerlabel.text = "OOoooppsss!!! Please Press on the right button !!"
                        }
//                            self.Structimg.image = UIImage(named:"sqr")
//                            self.ques2textview.isHidden = true
//                            self.ques1textview.isHidden = false
                            
                           // self.scorelabel.text = "Score is " + String(self.gameScore)
                    
                        
                    }
                }
                    
                    
                // button one action
                else if (choice == "C") {
                    
                    if(self.ques1==true) {
                        
                        DispatchQueue.main.async {
                            self.scorelabel.text = "Score is " + String(self.gameScore)
                        
                        self.Structimg.image = UIImage(named:"sqr")
                        self.ques2textview.isHidden = true
                        self.ques1textview.isHidden = false
                            self.answerlabel.text = "Please press the button on particle"

                            self.ques1 = false
                        }
                        
                   
                    }
                    else if (self.ques1==false)
                    {
                        
                        self.turnParticleRed()
                        DispatchQueue.main.async {
                            self.answerlabel.text = "OOoooppsss!!! Please Press on the right button !!"
                        }
//                        self.gameScore = self.gameScore + 1;
//                        DispatchQueue.main.async {
//                            self.scorelabel.text = "Score is " + String(self.gameScore)
                        //}
                        
                    }
                   
                }
                else if (choice == "D") {
                    
                    if(self.ques1==true) {
                        
                        self.turnParticleGreen()
                        self.gameScore = self.gameScore + 1;
                        DispatchQueue.main.async {
                            self.answerlabel.text = "YaaaY ...! your answer is correct !!"

                            self.scorelabel.text = "Score is " + String(self.gameScore)
                    }
                        
                
                    
                    }
                    else if (self.ques1==false)
                    {
                        
                        self.turnParticleRed()
                        DispatchQueue.main.async {
                            self.answerlabel.text = "OOoooppsss!!! Please Press on the right button !!"
                        }

                    }
                    
                    
                    
                }
                
               
            }
        })
    }
    
    
    
    func turnParticleGreen() {
        
        print("Pressed the change lights button")
        
        let parameters = ["green"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
    
    func turnParticleRed() {
        
        print("Pressed the change lights button")
        
        let parameters = ["red"]
        var task = myPhoton!.callFunction("answer", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn red")
            }
            else {
                print("Error when telling Particle to turn red")
            }
        }
        //var bytesToReceive : Int64 = task.countOfBytesExpectedToReceive
        
    }
   
    func  nextquestion() {
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "nextQuestion",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let choice = (event?.data)!
                    if (choice == "true") {
                       

                        
                        //self.turnParticleGreen()
//                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                        let secondview = storyBoard.instantiateViewController(withIdentifier: "nextView") as! secondviewController
//
//                        self.present(secondview, animated:true, completion:nil)

                        
                        
                    }
                    
                }
        })
        
    }
    
    @IBAction func testScoreButtonPressed(_ sender: Any) {
        
        print("score button pressed")
        
        // 1. Show the score in the Phone
        // ------------------------------
       // self.scoreLabel.text = "Score:\(self.gameScore)"
        
        // 2. Send score to Particle
        // ------------------------------
        let parameters = [String(self.gameScore)]
        var task = myPhoton!.callFunction("score", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to show score: \(self.gameScore)")
            }
            else {
                print("Error when telling Particle to show score")
            }
        }
        
        
        
        // 3. done!
        
        
    }
    
}

